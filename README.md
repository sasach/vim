# A Byte of Vim
## Insert mode
```
:e filename.txt //creates or starts editing a file
```
Press `i` to go into insert mode
Enter the text you want to add and then press `Esc`
```
:w //writes the entered text onto the file
:q //close the file
```
Commands:
```
i  //insert text just before the cursor
I  //insert text at the start of the line
a  //append text just after the cursor
A  //append text at the end of the line
o  //open a new line below
O  //open a new line above
s  //substitute the current character
S  //substitute the current line
r  //replace the current character
R  //replace continuous characters
```
Upper and lower case commands are  opposite in direction in which they open the line.
Help:
```
:help inserting
:help replacing
```
## Visual mode
Commands:
```
v  //visual mode selecting characters
V  //visual mode selecting entire lines
```
## Moving around
```
h  //move left
j  //move down
k  //move up
l  //move right
```
Precede with a number `n` to move that many places.
```
H  //jump as high as possible
M  //jump to the middle of the window
L  //jump as low as possible
```
Commands:
```
w  //move cursor to the next word
}  //move cursor to the next paragraph
{  //move to the prev paragraph
G  //preceded with `n` takes  to the nth line number
$  //jump to last character
e  //jump to the end of the next word
b  //move one word backward
)  //move to the next sentence
(  //move to the prev sentence
mx //mark a point, x can be replaced with a-zA-Z
'x //go back to the mark
ctrl-o  //prev location
ctrl-i  //next location
~  //toggle case of the selected text
```
## Editing basics
```
d  //cut selected text
y  //copy selected text
p  //paste copied/cut text
dl //delete current character
dw //delete current word
dd //delete current line
yap  //copy the current paragraph
p  //paste after current pos
P  //paste before current pos
xp //swap two characters
S  //substitute the whole line
u  //undo last change
ctrl-r  //redo last change
:earlier //takes you back in time before the changes were made
:later  //takes you forward in time after the changes were made
:undo n  //undo n number of changes
:undolist //view the undo tree
/word  //searches for occurences of 'word' in the file
/\<word\>  //searches exact occurences of 'word' in the file
/\d  //looks for a digit
:%s/x/y/gc  //finds all occurences of x and replaces with y after confirmation from the user
```
## Multiplicity
```
:set foldmethod=indent  //fold up indented text
zc  //fold foldable text
zo  //open folded text
ctrl-w  //switch between windows
:sp  //split the window into two parts with the same file
:vsp  //split the window into two parts vertically with the same file
:tabnew //open a new tab
gt  //go to next tab
gT  //go to previous tab
```
## Scripting
```
qa  //starts recording a MACRO script
q   //stops recording the MACRO script
@a  //applies the MACRO script to the entire line
```
## Programmers Editor
```
:set filetype=x  //replace x with python or vim or anyother lang for syntax highlighting
:set autoindent  //indent a line and enter this command to indent the lines that follow
%  //jumps to the next } if the cursor is placed on one {
:! //run a shell command
:sh //access a full shell
gf  //opens the file if cursor is placed on a filename in the code
:Vex or :Sex  //to browse the filesystem within Vim
Shift+j  //minifies the selected code
:%s/[{;}]/&\r/g|norm!=gg  //unminify && indent minified CSS
:set nu  //enable absolute line numbers for a file
```

